import IBill from "../interfaces/IBill";
import Grocery from "../../models/grocery";
import IGrocery from "../interfaces/IGrocery";

export default class GroceryService 
{
    FindDAsync = async (): Promise<IBill> =>
    {
        const tax = 0.18;
        let result = { numberOfArticles: 0, taxes: 0, totalPrice: 0}
        let groceries: IGrocery[] = await Grocery.find();
        groceries.forEach(x => {
            result.numberOfArticles += x.quantity;
        });
        groceries.forEach(x => {
            result.taxes += x.price * tax * x.quantity;
        });
        groceries.forEach(x => {
            result.totalPrice += x.price * x.quantity;
        });

        result.totalPrice = result.totalPrice + result.taxes;

        return result;
    }
};