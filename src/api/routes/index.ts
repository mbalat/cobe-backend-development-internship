import { Router } from "express";
import groceryRoute from "./GroceryRoute";

const router = Router();

router.use("/groceries", groceryRoute);

export default router;