import { Router } from "express";
import GroceryController from "../controllers/GroceryController";

const groceryController = new GroceryController(); 
const groceryRoute = Router();

groceryRoute.get("/", groceryController.FindAsync);

export default groceryRoute;