export default interface IGrocery 
{
    name: string;
    quantity: number;
    price: number;
}