export default interface IBill
{
    numberOfArticles: Number,
    taxes: Number,
    totalPrice: Number
}