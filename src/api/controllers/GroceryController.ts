import { Request, Response } from 'express';
import GroceryService from '../services/GroceryService';

export default class GroceryController extends GroceryService
{
    private _service: GroceryService = new GroceryService();

    FindAsync = async (req: Request, res: Response, next: any): Promise<Response> =>
    {
        const result = await this._service.FindDAsync();
        return res.status(200).json(result);
    }

}