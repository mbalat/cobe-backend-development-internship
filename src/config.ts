import dotenv from 'dotenv';
import path from 'path';

dotenv.config({
    path: path.resolve(`config/${process.env.NODE_ENV || 'development'}`)
});

const NODE_ENV = process.env.NODE_ENV || 'development';

const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 8000;
const DB_HOST = process.env.DB_HOST || 'localhost';
const DB_PORT = process.env.DB_PORT || 27017;
const DB_NAME = process.env.DB_NAME ||'Grocery';

export {
    NODE_ENV,
    HOST,
    PORT,
    DB_HOST,
    DB_PORT,
    DB_NAME
}