import express from 'express';
import router from './api/routes';
import { PORT, HOST, DB_HOST, DB_PORT, DB_NAME} from './config';
import mongoose from 'mongoose';

const baseUrl = "/cobe";

const app = express();

app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.use(baseUrl, router);

 mongoose.connect(`mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`);

(async () => {
    await app.listen(Number(PORT), HOST, () => {
        console.log(`http://${HOST}:${PORT}`);        
    })
})();