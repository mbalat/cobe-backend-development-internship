import mongoose, { Schema } from "mongoose";
import IGrocery from "../api/interfaces/IGrocery";

const grocerySchema = new Schema({
    id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    price: {
        type: Number,
        required: true
    }
});

export default mongoose.model<IGrocery>('Grocery', grocerySchema);